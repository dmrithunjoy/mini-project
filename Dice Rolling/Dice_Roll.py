import random

def roll(sides=6):
        num_rolling=random.randint(1, sides)
        return num_rolling

def main():
        sides=6
        rolling=True
        while rolling:
                roll_again=input('Ready To Play?  ENTER = Roll  Q = Quit')
                if roll_again.lower() != 'q':
                        num_rolling=roll(sides)
                        print('You Rolled a', num_rolling)
                else:
                        rolling=False

        print('Thanks For Playing')





main()
